package com.firstConnectMongo.springBootWithMongo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.firstConnectMongo.springBootWithMongo.exception.TodoCollectonException;
import com.firstConnectMongo.springBootWithMongo.model.TodoDTO;
import com.firstConnectMongo.springBootWithMongo.repository.TodoRepository;
import com.firstConnectMongo.springBootWithMongo.service.TodoService;

@RestController
public class TodoController {

	@Autowired
	private TodoRepository todoRepo;
	
	@Autowired
	private TodoService todoService;

	@GetMapping("/todos")
	public ResponseEntity<?> getAllTodo() {
		List<TodoDTO> todos = todoRepo.findAll();
		if (todos.size() > 0) {
			return new ResponseEntity<List<TodoDTO>>(todos, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("No todo available", HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/todos")
	public ResponseEntity<?> createTodo(@RequestBody TodoDTO todo) {
		try {
			todoService.createTodo(todo);
			return new ResponseEntity<TodoDTO>(todo, HttpStatus.OK);

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
		}catch (TodoCollectonException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
		}
	}

	@GetMapping("/todo/{id}")
	public ResponseEntity<?> getTodoById(@PathVariable("id") String id) {
		Optional<TodoDTO> todOptional = todoRepo.findById(id);
		if (todOptional.isPresent()) {
			return new ResponseEntity<>(todOptional.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Todo not found with id" + id, HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/todo/{id}")
	public ResponseEntity<?> updateTodoById(@PathVariable("id") String id, @RequestBody TodoDTO todo) {
		Optional<TodoDTO> todOptional = todoRepo.findById(id);
		if (todOptional.isPresent()) {
			TodoDTO todoToSave = todOptional.get();
			todoToSave.setCompleted(todo.getCompleted() != null ? todo.getCompleted() : todoToSave.getCompleted());
			todoToSave.setTodo(todo.getTodo() != null ? todo.getTodo() : todoToSave.getTodo());
			todoToSave.setDescription(
					todo.getDescription() != null ? todo.getDescription() : todoToSave.getDescription());
			todoToSave.setUpdatedAt(new Date(System.currentTimeMillis()));
			todoRepo.save(todoToSave);
			return new ResponseEntity<>(todoToSave, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Todo not found with id" + id, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/todo/{id}")
	public ResponseEntity<?> deleteTodoById(@PathVariable("id") String id) {
		try {

			todoRepo.deleteById(id);
			return new ResponseEntity<>("Successful delete with id" + id, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

}
