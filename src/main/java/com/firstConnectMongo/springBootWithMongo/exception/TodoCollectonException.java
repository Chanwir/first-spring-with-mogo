package com.firstConnectMongo.springBootWithMongo.exception;

public class TodoCollectonException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TodoCollectonException(String message) {
		super(message);
	
	}
	
	public static String NotFoundException(String id) {
		return "Todo with id "+id+" not found!!";
	}

	public static String TodoAlredyExists() {
		return "Todo with given name alredy exists!!";
	}
	
	
}
