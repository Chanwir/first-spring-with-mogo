package com.firstConnectMongo.springBootWithMongo.service;

import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.firstConnectMongo.springBootWithMongo.exception.TodoCollectonException;
import com.firstConnectMongo.springBootWithMongo.model.TodoDTO;
import com.firstConnectMongo.springBootWithMongo.repository.TodoRepository;

@Service
public class TodoServiceImpl implements TodoService {
	
	@Autowired
	private TodoRepository todoRepository;	
	@Override
	public void createTodo(TodoDTO todo) throws ConstraintViolationException,TodoCollectonException {
		Optional<TodoDTO> todOptional = todoRepository.findByTodo(todo.getTodo());
		if(todOptional.isPresent()) {
			throw new TodoCollectonException(TodoCollectonException.TodoAlredyExists());
		} else {
			todo.setCreatedAt(new Date(System.currentTimeMillis()));
			todoRepository.save(todo);
		}
	}
}
