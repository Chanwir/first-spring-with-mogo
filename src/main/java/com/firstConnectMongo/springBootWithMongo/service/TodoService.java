package com.firstConnectMongo.springBootWithMongo.service;

import javax.validation.ConstraintViolationException;

import com.firstConnectMongo.springBootWithMongo.exception.TodoCollectonException;
import com.firstConnectMongo.springBootWithMongo.model.TodoDTO;

public interface TodoService {
	public void createTodo(TodoDTO todo) throws ConstraintViolationException,TodoCollectonException;
}
